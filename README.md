# Syntelix MQTT Subscriber

This is a simple app to solve the need of getting boats information from a MQTT queue, store it in a relational database
and expose it through some API endpoints.

It gets subscribed from the beginning to the topic `boats/+/positions` with the QOS `0` and checks each message's
integrity. If the message passes the structure validation, it will be stored in the database as a `BoatPosition` and
otherwise as a `UnexpectedMessage`.

`BoatPosition` is a structure with a IMO, latitude, longitude and a timestamp.

`UnexpectedMessage` has a more general structure in which are stored all the messages that are published to the current
topic but don't fit with the `BoatPosition` structure.

To access to this data, there is an [API](http://localhost:8081/api/) with some endpoints:

- http://localhost:8081/api/boat_positions
- http://localhost:8081/api/unexpected_messages

## Requirements

- Docker
- Docker Compose

## Installation

Clone the repository from Bitbucket or download its sources:

````
git clone https://vinopatxoko@bitbucket.org/vinopatxoko/syntelix-mqtt-subscriber.git
````

Go to the project root folder and use Docker to start. The first time, it will download all the required stuff and build
the images to bring up the project. Next time it will pick these built images and run it in a hurry.

````
docker-compose up -d
````

That's all, the project should be running. Due to the command that is executed when the container starts, the
application should have been subscribed to the topic `boats/+/positions` and the QOS `0`. There should also be some API
endpoints available at [http://localhost:8081/api](http://localhost:8081/api).

## Configuration

A basic configuration can be found in the `.env` file of the root folder.

````
MQTT_HOST=6e437b1ad8c2452f971c8a55fc644b91.s1.eu.hivemq.cloud
MQTT_PORT=8883
MQTT_USER=publisher
MQTT_PASS=s3qFAbyT7T
MQTT_CA_PATH=/etc/ssl/certs/ca-certificates.crt
MQTT_SSL_ENABLE=1
````

All the keys starting with `MQTT` refer to the connection to the MQTT broker. The `MQTT_CA_PATH` key indicates the path
for the certificates to check with the broker and `MQTT_SSL_ENABLE` is a switch between 0 or 1 and tells the app if its
necessary to use the certificate or not.

## General operations

The console commands need to be run on the PHP container. To avoid accessing the container, it can be used `docker exec`
from the host machine. To get it even easier, it is very handy to get the container name from here:

````
docker ps

CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS          PORTS                                                  NAMES
ff7fa2be74fe   mqtt-subscriber-nginx   "/docker-entrypoint.…"   13 minutes ago   Up 13 minutes   0.0.0.0:8081->80/tcp, :::8081->80/tcp                  syntelix-mqtt-subscriber_nginx_1
e4c856f234f3   mqtt-subscriber-php     "docker-php-entrypoi…"   13 minutes ago   Up 13 minutes   9000/tcp                                               syntelix-mqtt-subscriber_php_1
bf5082fba182   mysql:8.0               "docker-entrypoint.s…"   13 minutes ago   Up 13 minutes   0.0.0.0:3306->3306/tcp, :::3306->3306/tcp, 33060/tcp   syntelix-mqtt-subscriber_mysql_1
````

It should be `syntelix-mqtt-subscriber_php_1` by default, but changing the configuration or running more containers at
the same time can change it. The following examples are using this name.

### Message subscribing

This command subscribes to the topic `boats/+/positions` and the QOS `0`. It is executed automatically when Docker goes
up and it never ends, soo it shouldn't be necessary to call it again.

````
docker exec -it syntelix-mqtt-subscriber_php_1 php bin/console syntelix:subscribe
````

### Tests

There are some tests of the application that you can run with this:

````
docker exec -it syntelix-mqtt-subscriber_php_1 php bin/phpunit
````
