<?php
namespace App\Service;

use App\Entity\BoatPosition;
use App\Model\MqttTopic;

class MqttBoatPositionSubscriber extends MqttSubscriber
{
    /**
     * @param MqttTopic[] $mqttTopics
     */
    public function subscribe(array $mqttTopics = []): bool
    {
        $mqttTopic = new MqttTopic('boats/+/positions', 0);
        $this->mqttTopics = [$mqttTopic];
        return $this->start();
    }

    protected function getExpectedEntityClass(): string
    {
        return BoatPosition::class;
    }
}