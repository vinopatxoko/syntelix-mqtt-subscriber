<?php
namespace App\Service;

use App\Entity\UnexpectedMessage;
use App\Model\MqttTopic;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Mosquitto\Client;
use Mosquitto\Message;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MqttSubscriber
{
    /**
     * @var MqttTopic[]
     */
    protected array $mqttTopics;
    protected Client $client;
    protected ValidatorInterface $validator;
    protected SerializerInterface $serializer;
    protected EntityManagerInterface $entityManager;

    public function __construct(
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager
    ) {
        $this->validator = $validator;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
        $this->client = new Client();
        if ($_ENV['MQTT_SSL_ENABLE']) $this->client->setTlsCertificates($_ENV['MQTT_CA_PATH']);
        $this->client->setCredentials($_ENV['MQTT_USER'], $_ENV['MQTT_PASS']);
        $this->client->onConnect([$this, 'handleOnConnect']);
        $this->client->onMessage([$this, 'handleOnMessage']);
    }

    /**
     * @param MqttTopic[] $mqttTopics
     */
    public function subscribe(array $mqttTopics = []): bool
    {
        $this->mqttTopics = $mqttTopics;
        return $this->start();
    }

    public function handleOnConnect(int $code)
    {
        if ($code === 0) {
            foreach ($this->mqttTopics as $mqttTopic) {
                $this->client->subscribe($mqttTopic->getTopic(), $mqttTopic->getQos());
            }
        }
    }

    public function handleOnMessage(Message $message)
    {
        try {
            $expectedEntity = $this->serializer->deserialize($message->payload, $this->getExpectedEntityClass(), 'json');
            $errors = $this->validator->validate($expectedEntity);
            if (!count($errors)) {
                $this->entityManager->persist($expectedEntity);
                $this->entityManager->flush();
            } else {
                $this->saveUnexpectedMessage($message);
            }
        } catch (Exception $exception) {
            $this->saveUnexpectedMessage($message);
        }
    }

    protected function getExpectedEntityClass(): string
    {
        return '';
    }

    protected function start(): bool
    {
        $succeeded = true;
        try {
            $this->client->connect($_ENV['MQTT_HOST'], $_ENV['MQTT_PORT']);
            $this->client->loopForever();
        } catch (Exception $exception) {
            $succeeded = false;
        }
        return $succeeded;
    }

    protected function saveUnexpectedMessage(Message $message)
    {
        $unexpectedMessage = new UnexpectedMessage($message);
        $errors = $this->validator->validate($unexpectedMessage);
        if (!count($errors)) {
            $this->entityManager->persist($unexpectedMessage);
            $this->entityManager->flush();
        }
    }
}