<?php
namespace App\Factory;

use App\Entity\UnexpectedMessage;
use App\Repository\UnexpectedMessageRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<UnexpectedMessage>
 *
 * @method static UnexpectedMessage|Proxy createOne(array $attributes = [])
 * @method static UnexpectedMessage[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static UnexpectedMessage|Proxy find(object|array|mixed $criteria)
 * @method static UnexpectedMessage|Proxy findOrCreate(array $attributes)
 * @method static UnexpectedMessage|Proxy first(string $sortedField = 'id')
 * @method static UnexpectedMessage|Proxy last(string $sortedField = 'id')
 * @method static UnexpectedMessage|Proxy random(array $attributes = [])
 * @method static UnexpectedMessage|Proxy randomOrCreate(array $attributes = [])
 * @method static UnexpectedMessage[]|Proxy[] all()
 * @method static UnexpectedMessage[]|Proxy[] findBy(array $attributes)
 * @method static UnexpectedMessage[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static UnexpectedMessage[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static UnexpectedMessageRepository|RepositoryProxy repository()
 * @method UnexpectedMessage|Proxy create(array|callable $attributes = [])
 */
final class UnexpectedMessageFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
            'topic' => self::faker()->text(),
            'payload' => self::faker()->text(),
            'qos' => self::faker()->randomNumber(),
            'retain' => self::faker()->boolean(),
            'timestamp' => self::faker()->randomNumber(),
        ];
    }

    protected function initialize(): self
    {
        return $this;
    }

    protected static function getClass(): string
    {
        return UnexpectedMessage::class;
    }
}
