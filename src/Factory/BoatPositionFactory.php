<?php
namespace App\Factory;

use App\Entity\BoatPosition;
use App\Repository\BoatPositionRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<BoatPosition>
 *
 * @method static BoatPosition|Proxy createOne(array $attributes = [])
 * @method static BoatPosition[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static BoatPosition|Proxy find(object|array|mixed $criteria)
 * @method static BoatPosition|Proxy findOrCreate(array $attributes)
 * @method static BoatPosition|Proxy first(string $sortedField = 'id')
 * @method static BoatPosition|Proxy last(string $sortedField = 'id')
 * @method static BoatPosition|Proxy random(array $attributes = [])
 * @method static BoatPosition|Proxy randomOrCreate(array $attributes = [])
 * @method static BoatPosition[]|Proxy[] all()
 * @method static BoatPosition[]|Proxy[] findBy(array $attributes)
 * @method static BoatPosition[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static BoatPosition[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static BoatPositionRepository|RepositoryProxy repository()
 * @method BoatPosition|Proxy create(array|callable $attributes = [])
 */
final class BoatPositionFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
            'imo' => self::faker()->text(),
            'timestamp' => self::faker()->randomNumber(),
            'latitude' => self::faker()->latitude(),
            'longitude' => self::faker()->longitude(),
        ];
    }

    protected function initialize(): self
    {
        return $this;
    }

    protected static function getClass(): string
    {
        return BoatPosition::class;
    }
}
