<?php
namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UnexpectedMessageRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Mosquitto\Message;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"},
 *     normalizationContext={"groups"={"unexpected_message:read"}, "swagger_definition_name"="Read"}
 * )
 * @ORM\Entity(repositoryClass=UnexpectedMessageRepository::class)
 * @UniqueEntity(fields={"topic","payload","qos","retain"})
 */
class UnexpectedMessage
{
    /**
     * @Groups({"unexpected_message:read"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;
    /**
     * @Groups({"unexpected_message:read"})
     * @ORM\Column(type="string")
     */
    private string $topic;
    /**
     * @Groups({"unexpected_message:read"})
     * @ORM\Column(type="string")
     */
    private string $payload;
    /**
     * @Groups({"unexpected_message:read"})
     * @ORM\Column(type="integer")
     */
    private int $qos;
    /**
     * @Groups({"unexpected_message:read"})
     * @ORM\Column(type="boolean")
     */
    private bool $retain;
    /**
     * @Groups({"unexpected_message:read"})
     * @ORM\Column(type="integer")
     */
    private int $timestamp;

    public function __construct(Message $message)
    {
        $this->topic = $message->topic;
        $this->payload = $message->payload;
        $this->qos = $message->qos;
        $this->retain = $message->retain;
        $datetime = new DateTime();
        $this->timestamp = $datetime->getTimestamp();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTopic(): ?string
    {
        return $this->topic;
    }

    public function setTopic(string $topic)
    {
        $this->topic = $topic;
    }

    public function getPayload(): ?string
    {
        return $this->payload;
    }

    public function setPayload(string $payload)
    {
        $this->payload = $payload;
    }

    public function getQos(): ?int
    {
        return $this->qos;
    }

    public function setQos(int $qos)
    {
        $this->qos = $qos;
    }

    public function isRetain(): ?bool
    {
        return $this->retain;
    }

    public function setRetain(bool $retain)
    {
        $this->retain = $retain;
    }

    public function getTimestamp(): ?int
    {
        return $this->timestamp;
    }

    public function setTimestamp(int $timestamp)
    {
        $this->timestamp = $timestamp;
    }
}
