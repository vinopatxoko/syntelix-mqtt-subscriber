<?php
namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BoatPositionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"},
 *     normalizationContext={"groups"={"boat_position:read"}, "swagger_definition_name"="Read"}
 * )
 * @ORM\Entity(repositoryClass=BoatPositionRepository::class)
 * @UniqueEntity(fields={"imo", "timestamp"})
 */
class BoatPosition
{
    /**
     * @Groups({"boat_position:read"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;
    /**
     * @Groups({"boat_position:read"})
     * @ORM\Column(type="string")
     */
    private string $imo;
    /**
     * @Groups({"boat_position:read"})
     * @ORM\Column(type="integer")
     */
    private int $timestamp;
    /**
     * @Groups({"boat_position:read"})
     * @ORM\Column(type="float")
     */
    private float $latitude;
    /**
     * @Groups({"boat_position:read"})
     * @ORM\Column(type="float")
     */
    private float $longitude;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImo(): ?string
    {
        return $this->imo;
    }

    public function setImo(string $imo)
    {
        $this->imo = $imo;
    }

    public function getTimestamp(): ?int
    {
        return $this->timestamp;
    }

    public function setTimestamp(int $timestamp)
    {
        $this->timestamp = $timestamp;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude)
    {
        $this->latitude = $latitude;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude)
    {
        $this->longitude = $longitude;
    }
}
