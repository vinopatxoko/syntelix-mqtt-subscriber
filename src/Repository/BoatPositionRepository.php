<?php
namespace App\Repository;

use App\Entity\BoatPosition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BoatPosition|null find($id, $lockMode = null, $lockVersion = null)
 * @method BoatPosition|null findOneBy(array $criteria, array $orderBy = null)
 * @method BoatPosition[]    findAll()
 * @method BoatPosition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoatPositionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BoatPosition::class);
    }
}
