<?php
namespace App\Repository;

use App\Entity\UnexpectedMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UnexpectedMessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method UnexpectedMessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method UnexpectedMessage[]    findAll()
 * @method UnexpectedMessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnexpectedMessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UnexpectedMessage::class);
    }
}
