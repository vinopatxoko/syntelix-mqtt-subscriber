<?php
namespace App\Command;

use App\Service\MqttBoatPositionSubscriber;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SubscribeCommand extends Command
{
    protected static $defaultName = 'syntelix:subscribe';
    protected static $defaultDescription = 'Retrieves the position of a boat from a MQTT queue';
    private MqttBoatPositionSubscriber $mqttSubscriber;

    public function __construct(
        MqttBoatPositionSubscriber $mqttSubscriber
    ) {
        parent::__construct();
        $this->mqttSubscriber = $mqttSubscriber;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        while (!$this->mqttSubscriber->subscribe()) {
            $io->warning('An error has occurred. Trying to reconnect...');
            sleep(5);
        }
        return Command::SUCCESS;
    }
}
