<?php
namespace App\Model;

class MqttTopic
{
    private string $topic;
    private int $qos;

    public function __construct(string $topic, int $qos)
    {
        $this->topic = $topic;
        $this->qos = $qos;
    }

    public function getTopic(): string
    {
        return $this->topic;
    }

    public function getQos(): int
    {
        return $this->qos;
    }
}