#!/bin/bash

composer install --working-dir={VAR_PROJECT_PATH}
chmod -R 777 {VAR_PROJECT_PATH}/var
chmod -R 777 {VAR_PROJECT_PATH}/bin

sleep 10
php {VAR_PROJECT_PATH}/bin/console doctrine:database:create --quiet
php {VAR_PROJECT_PATH}/bin/console doctrine:migrations:migrate --no-interaction

php-fpm --daemonize

php {VAR_PROJECT_PATH}/bin/console syntelix:subscribe