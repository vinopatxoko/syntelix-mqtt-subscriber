<?php
namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\BoatPosition;
use App\Factory\BoatPositionFactory;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class BoatPositionResourceTest extends ApiTestCase
{
    use Factories, ResetDatabase;

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetCollection()
    {
        BoatPositionFactory::createMany(10);
        static::createClient()->request('GET', '/api/boat_positions');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@context' => '/api/contexts/BoatPosition',
            '@id' => '/api/boat_positions',
            '@type' => 'hydra:Collection',
        ]);
        $this->assertMatchesResourceCollectionJsonSchema(BoatPosition::class);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testGetItem()
    {
        $boatPosition = BoatPositionFactory::createOne();
        $endpoint = sprintf('/api/boat_positions/%d', $boatPosition->getId());
        static::createClient()->request('GET', $endpoint);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@context' => '/api/contexts/BoatPosition',
            '@id' => $endpoint,
            '@type' => 'BoatPosition',
        ]);
        $this->assertMatchesResourceItemJsonSchema(BoatPosition::class);
    }
}