<?php
namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\UnexpectedMessage;
use App\Factory\UnexpectedMessageFactory;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class UnexpectedMessageResourceTest extends ApiTestCase
{
    use Factories, ResetDatabase;

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetCollection()
    {
        UnexpectedMessageFactory::createMany(10);
        static::createClient()->request('GET', '/api/unexpected_messages');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@context' => '/api/contexts/UnexpectedMessage',
            '@id' => '/api/unexpected_messages',
            '@type' => 'hydra:Collection',
        ]);
        $this->assertMatchesResourceCollectionJsonSchema(UnexpectedMessage::class);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testGetItem()
    {
        $unexpectedMessage = UnexpectedMessageFactory::createOne();
        $endpoint = sprintf('/api/unexpected_messages/%d', $unexpectedMessage->getId());
        static::createClient()->request('GET', $endpoint);
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@context' => '/api/contexts/UnexpectedMessage',
            '@id' => $endpoint,
            '@type' => 'UnexpectedMessage',
        ]);
        $this->assertMatchesResourceItemJsonSchema(UnexpectedMessage::class);
    }
}