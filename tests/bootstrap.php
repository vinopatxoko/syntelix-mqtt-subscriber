<?php

use Symfony\Component\Dotenv\Dotenv;

require dirname(__DIR__) . '/vendor/autoload.php';
if (file_exists(dirname(__DIR__) . '/config/bootstrap.php')) {
    require dirname(__DIR__) . '/config/bootstrap.php';
} else if (method_exists(Dotenv::class, 'bootEnv')) {
    (new Dotenv())->bootEnv(dirname(__DIR__) . '/.env');
}
passthru('php ' . __DIR__ . '/../bin/console doctrine:schema:drop --env=test --force --quiet');
passthru('php ' . __DIR__ . '/../bin/console doctrine:schema:create --env=test --quiet');